module.exports = function(eleventyConfig) {
    eleventyConfig.addPassthroughCopy('./src/public/css');
    eleventyConfig.addPassthroughCopy('./src/public/fonts')
    eleventyConfig.addPassthroughCopy('./src/public/js');
    eleventyConfig.addPassthroughCopy('./src/public/images');

    return {
        dir: {
            input: "src",
            output: "_site",
            data: "_data",
        },
        templateFormats: ["html", "njk", "md", "11ty.js"],
		markdownTemplateEngine: "njk",
		htmlTemplateEngine: "njk",
    };
}